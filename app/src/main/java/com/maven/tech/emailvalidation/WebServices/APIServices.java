package com.maven.tech.emailvalidation.WebServices;


import com.maven.tech.emailvalidation.Models.EmailVerificationResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Ishaq on 2/4/2017.
 */

public interface APIServices {


    @GET("/verify")
    public void getEmailVerified(@Query("email") String email, @Query("apikey") String api_key, Callback<EmailVerificationResponse> callback);

}
