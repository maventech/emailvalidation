package com.maven.tech.emailvalidation.WebServices;


/**
 * Created by Ishaq on 2/3/2017.
 */

public class RetrofitRestClient {
    private retrofit.RestAdapter restAdapter;
    public static final String URL = "https://api.kickbox.com/v2";

    public RetrofitRestClient(String url) {
        restAdapter = new retrofit.RestAdapter.Builder()
                .setEndpoint(url)
                .setLogLevel(retrofit.RestAdapter.LogLevel.FULL)
                .build();
    }


    public APIServices getService() {
        return restAdapter.create(APIServices.class);
    }


}
