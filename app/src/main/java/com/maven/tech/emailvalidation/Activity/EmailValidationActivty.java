package com.maven.tech.emailvalidation.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.maven.tech.emailvalidation.Models.EmailVerificationResponse;
import com.maven.tech.emailvalidation.R;
import com.maven.tech.emailvalidation.Utility.CustomEmailFilterAdapter;
import com.maven.tech.emailvalidation.Utility.Helper;
import com.maven.tech.emailvalidation.Utility.InternetDetector;
import com.maven.tech.emailvalidation.Utility.Validation;
import com.maven.tech.emailvalidation.WebServices.RetrofitRestClient;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class EmailValidationActivty extends AppCompatActivity {

    Button mBtnSubmitEmail;
    // UI references.
    private AutoCompleteTextView mEmailView;
    private ProgressDialog mProgressDialog;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_validation);
        context = this;
        getMarshmallowPermission();
        initViews();
        addListeners();
    }

    private void addListeners() {
        btnSubmitOnClickListener();
    }

    private void btnSubmitOnClickListener() {
        mBtnSubmitEmail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Validation.checkSpace(mEmailView.getText().toString())) {
                    if (Validation.isEmailValid(mEmailView.getText().toString(), context)) {
                        if (InternetDetector.checkIntern(context))
                            submitEmail();
                        else
                            showDialog("Error", getString(R.string.internet_connection_msg));
                    } else {
                        mEmailView.setError(getString(R.string.valid_email_address_msg));
                        Log.d("EMail", "onClick: " + mEmailView.getText().toString());
                    }
                } else {
                    mEmailView.setError(getString(R.string.remove_space_msg));

                }
            }
        });

    }

    public void submitEmail() {
        try {
            mProgressDialog.setMessage(getString(R.string.emial_address_verify_msg));
            mProgressDialog.show();
            RetrofitRestClient mRetrofitRestClient = new RetrofitRestClient(RetrofitRestClient.URL);
            mRetrofitRestClient.getService().getEmailVerified(mEmailView.getText().toString(), getString(R.string.api_key), new Callback<EmailVerificationResponse>() {

                @Override
                public void success(final EmailVerificationResponse verificationResponse, Response response) {
                    mProgressDialog.dismiss();
                    if (verificationResponse.isSuccess() && verificationResponse.getResult().equals("deliverable")) {
                        showDialog("Success", getString(R.string.email_success_msg));
                    } else {
                        showDialog("Error", getString(R.string.email_notfound_msg));
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    mProgressDialog.dismiss();
                    Log.i("Error:", "" + error.getMessage().toString());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })

                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void initViews() {
        mProgressDialog = new ProgressDialog(context);
        mEmailView = findViewById(R.id.email);
        mBtnSubmitEmail = findViewById(R.id.email_sign_in_button);
        populateEmailViewAutoComplete();
    }

    private void populateEmailViewAutoComplete() {
        CustomEmailFilterAdapter adapter = new CustomEmailFilterAdapter(this, android.R.layout.simple_list_item_1, new ArrayList<String>(Arrays.asList(Helper.email_domains)));
        mEmailView.setAdapter(adapter);
    }

    private void getMarshmallowPermission() {
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
        }
//        ActivityCompat.requestPermissions(EmailValidationActivty.this,
//                new String[]{Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE},
//                1);
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(EmailValidationActivty.this,
                new String[]{Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE},
                1);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(EmailValidationActivty.this, R.string.permission_denied_msg, Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

}

