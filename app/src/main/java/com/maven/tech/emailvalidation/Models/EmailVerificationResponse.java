package com.maven.tech.emailvalidation.Models;

/**
 * Created by Hammad Ali Khan on 5/13/2018.
 */

public class EmailVerificationResponse {
    private String result;
    private String reason;
    private boolean success;
    private String message;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "EmailVerificationResponse{" +
                "result='" + result + '\'' +
                ", reason='" + reason + '\'' +
                ", success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}
