package com.maven.tech.emailvalidation.Utility;

import android.content.Context;
import android.util.Patterns;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sabaq on 5/10/2018.
 */

public class Validation {
    String emailText;

    public Validation() {
    }

    public Validation(String email) {
        this.emailText = email;
    }

    public static boolean isEmailValid(String email,Context context){
        Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(email);
        return matcher.matches();
    }


    public static boolean
    isEmailFormateValid(String email, Context context) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        } else {
            Toast.makeText(context, "Email is not valid", Toast.LENGTH_SHORT).show();
        }
        return isValid;
    }

    public static boolean checkSpace(String text) {
        if (text.toString().contains(" ")) {
            return true;
        } else {
            return false;
        }
    }

    public static  boolean isDomainValid(CharSequence text) {
        return text != null && android.util.Patterns.EMAIL_ADDRESS.matcher(text).matches();

    }
    
}
